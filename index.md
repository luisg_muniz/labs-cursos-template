---
title: Plantilla para cursos
description: Curso genérico
permalink: /
---

# Curso genérico

* Transparencias
  * Tema {% attach_file {"file_name": "/slides_pdf/01-introduccion.pdf", "title":"01: Introducción"} %}


* Documentación pública
  * {TBD}

* Enlaces extra
  * {TBD}

* Documentación extra
  * {TBD}
