---
title: Curso genérico
subtitle: Introducción
---

# Ejemplos de contenido

  * Lista
  * Incremental
  * Los elementos se van desvelando...
  * ...pero en todo momento son visibles


# No incrementales
::: nonincremental
# Nonincremental
  * También se pueden mostrar
  * todos
  * de
  * golpe
:::


# Imágenes (I)

![Sus créditos, en las notas](attached_files/images/side-eyeing-chloe.jpg)


# Imágenes (I)

![Be visual!](attached_files/images/krustjuggle.png){width=85px height=112px}


# Imágenes (I)

![Be \textbf{more} visual!](attached_files/images/drake-about-namespaces.png)


# Desvelando por partes arbitrarias

::: nonincremental
  * ~~"Pues en mi máquina funciona"~~ \pause$\leadsto$ Portabilidad\pause
  * Contenedores \pause = estandarización\pause
    * ...y después, \pause escalabilidad\pause
:::


# Cuidado con la tipografía

  * No abusar de `texttt`
  * Simple $\leadsto$ (~~`root`~~)
  * systemctl $\Bigl\{$enable$\Big|$start$\Bigr\}$ docker
  * bar baz $\bigl[ ... \bigr]$ \--long--option
  * docker system prune $\bigl[$--a$\bigr]$
  * docker build -t usu/img:1.0 $\Large\boldsymbol{.}$
  * $\sim$/.kube/config


# Code blocks

```dockerfile
  - FROM ubuntu:22.04
  - FROM ruby:3.0
  - FROM scratch
```
\pause{Ayudan, pero mejor no abusar}


# ¿Preguntas? {.plain}

  \center{\textcolor{Mahogany}{\Huge$\spadesuit$}}


::: notes
* Créditos de las imágenes:
  * https://knowyourmeme.com/memes/side-eyeing-chloe
  * https://imgflip.com/i/81h7z8
  * http://www.juggling.org/pics/cartoons.html
:::
